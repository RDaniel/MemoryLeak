import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobsComponent } from './jobs/jobs.component';
import { CompaniesComponent } from './companies/companies.component';
import { JobItemComponent } from './job-item/job-item.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { RegisterPersonComponent } from './register-person/register-person.component';
import { RegisterFirmComponent } from './register-firm/register-firm.component';
import { LoginComponent } from './login/login.component';
import { AdvertCreateComponent } from './advert-create/advert-create.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { AdminDashboardComponent } from './admin-page/admin-dashboard/admin-dashboard.component';
import { AdminRecensionsComponent } from './admin-page/admin-recensions/admin-recensions.component';
import { AdminUsersComponent } from './admin-page/admin-users/admin-users.component';
import { AdminAdvertisementComponent } from './admin-page/admin-advertisement/admin-advertisement.component';
import { AdminJobtypesComponent } from './admin-page/admin-jobtypes/admin-jobtypes.component';
import { AuthGuardService } from './services/auth-guard.service';
import { EditFirmComponent } from './edit-firm/edit-firm.component';
import { EditPersonComponent } from './edit-person/edit-person.component';
import { UserComponent } from './user/user.component';
import { MessagesComponent } from './user/messages/messages.component';
import { PersonalInfoComponent } from './user/personal-info/personal-info.component';
import { MyAdvertisementsComponent } from './user/my-advertisements/my-advertisements.component';
import { ConversationComponent } from './user/conversation/conversation.component';
import { NewConversationComponent } from './user/new-conversation/new-conversation.component';
import { RoleGuardService } from './services/role-guard.service';

const appRoutes: Routes = [
    { path: '', redirectTo: '/poslovi', pathMatch: 'full'},
    { path: 'poslovi', component: JobsComponent},
    { path: 'novi-oglas', component: AdvertCreateComponent, canActivate: [AuthGuardService]},
    { path: 'poslovi/:id', component: JobItemComponent},
    { path: 'tvrtke', component: CompaniesComponent},
    { path: 'tvrtke/:id', component: CompanyDetailsComponent},
    { path: 'registriraj-osobu', component: RegisterPersonComponent},
    { path: 'registriraj-tvrtku', component: RegisterFirmComponent},
    { path: 'prijava', component: LoginComponent},
    {
        path: 'administrator',
        component: AdminPageComponent,
        children: [
          { path: '', redirectTo: 'admin-pregled', pathMatch: 'full' },
          { path: 'admin-oglasi', component: AdminAdvertisementComponent },
          { path: 'admin-pregled', component: AdminDashboardComponent },
          { path: 'admin-recenzije', component: AdminRecensionsComponent },
          { path: 'admin-korisnici', component: AdminUsersComponent },
          { path: 'admin-kategorije', component: AdminJobtypesComponent },
        ],
        canActivate: [RoleGuardService]
    },
    { path: 'korisnik/:id/podaci/uredi-tvrtku', component: EditFirmComponent, canActivate: [AuthGuardService]},
    { path: 'korisnik/:id/podaci/uredi-osobu', component: EditPersonComponent, canActivate: [AuthGuardService]},
    {
        path: 'korisnik/:id',
        component: UserComponent,
        children: [
            { path: '', redirectTo: 'podaci', pathMatch: 'full'},
            { path: 'podaci', component: PersonalInfoComponent},
            { path: 'moji-oglasi', component: MyAdvertisementsComponent},
            { path: 'razgovori', component: MessagesComponent},
            { path: 'razgovori/novi', component: NewConversationComponent},
            { path: 'razgovori/:id', component: ConversationComponent},
        ],
        canActivate: [AuthGuardService]
    },
    { path: '**', redirectTo: '/poslovi', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
