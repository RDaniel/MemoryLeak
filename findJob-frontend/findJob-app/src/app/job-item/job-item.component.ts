import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Job } from '../models/job.model';
import { AdvertisementService } from '../services/advertisement.service';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { ConversationService } from '../services/conversation.service';
import { Conversation } from '../models/conversation.model';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-job-item',
  templateUrl: './job-item.component.html',
  styleUrls: ['./job-item.component.css']
})
export class JobItemComponent implements OnInit {
  job: Job;
  receieverId: number;
  userId: number;
  adId: number;

  constructor(private route: ActivatedRoute, private adService: AdvertisementService, private authService: AuthService,
    private router: Router, private userService: UserService, private convService: ConversationService) { }

  ngOnInit() {
    this.adId = this.route.snapshot.params['id'];
    this.adService.getJob(this.adId).subscribe(
      (job: Job) => {
        this.job = job;
        this.receieverId = this.job.owner.id;
      }
    );
    this.userService.getCurrentUser().subscribe(
      (user: User) => {
        this.userId = user.id;
      },
      err => console.log('Prijavite se da bi kontaktirali oglašivača')
    );
  }

  newConversation() {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['prijava']);
    } else if (this.receieverId !== this.userId) {
      this.convService.getConversationByAdId(this.adId).subscribe(
        (conv: Conversation) => {
          this.router.navigate(['korisnik/' + this.userId + '/razgovori/' + conv.id]);
        },
        err => {
          this.router.navigate(['korisnik/' + this.userId + '/razgovori/novi'], {
            queryParams: {
              primatelj: this.receieverId, oglas: this.adId
            }
          });
        }
      );
    } else {
      alert('Nije moguće poslati poruku samome sebi.');
    }
  }

  onCompanyName() {
    this.router.navigate(['tvrtke/' + this.job.owner.id]);
  }
}
