import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Company } from '../models/company.model';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { JobtypeService } from '../services/jobtype.service';
import { Jobtype } from '../models/jobtype.model';

@Component({
  selector: 'app-register-firm',
  templateUrl: './register-firm.component.html',
  styleUrls: ['./register-firm.component.css']
})
export class RegisterFirmComponent implements OnInit {
  user: User;
  jobTypes: Jobtype[];

  constructor(private userService: UserService, private authService: AuthService,
    private jobTypeService: JobtypeService) { }

  ngOnInit() {
    this.jobTypeService.getAllJobtypes().subscribe(
      (data: any[]) => {
        this.jobTypes = data;
      }
    );
  }

  onSubmit(form: NgForm) {
    this.userService.getByUsername(form.value.userData.username).subscribe(
      (user: any) => {
        if (user === null) {
          this.register(form);
        } else {
          alert('Odabrano korisničko ime već postoji.');
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  register(form: NgForm) {
    if (form.value.userData.oib === '') { form.value.userData.oib = null; }
    if (form.value.userData.address === '') { form.value.userData.address = null; }
    if (form.value.userData.phoneNumber === '') { form.value.userData.phoneNumber = null; }
    if (form.value.logo === '') { form.value.logo = null; }
    console.log('New company has been submitted');
    this.user = new Company(null, 'company', form.value.userData.username, form.value.userData.password,
      form.value.userData.email, form.value.userData.oib, form.value.userData.address, form.value.userData.phoneNumber,
      null, null, ['COMPANY'], form.value.companyName, form.value.description, form.value.logo, null, form.value.category);
    this.userService.registerUser(this.user).subscribe(
      data => this.authService.login(form.value.userData.username, form.value.userData.password),
      err => console.log(err)
    );
  }
}
