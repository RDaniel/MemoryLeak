import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Company } from '../models/company.model';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-firm',
  templateUrl: './edit-firm.component.html',
  styleUrls: ['./edit-firm.component.css']
})
export class EditFirmComponent implements OnInit {
  user: Company;

  constructor(private userService: UserService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(
      (user: Company) => {
        this.user = user;
      }
    );
  }

  onSubmit(form: NgForm) {
    if (form.value.userData.eMail === '') {form.value.userData.eMail = this.user.eMail; }
    if (form.value.userData.oib === '') {form.value.userData.oib = this.user.oib; }
    if (form.value.userData.address === '') {form.value.userData.address = this.user.address; }
    if (form.value.userData.phoneNumber === '') {form.value.userData.phoneNumber = this.user.phoneNumber; }
    if (form.value.companyName === '') {form.value.companyName = this.user.companyName; }
    if (form.value.description === '') {form.value.description = this.user.description; }
    if (form.value.logoName === '') {form.value.logoName = this.user.logoName; }
    console.log('Company edit has been submitted');
    this.user = new Company(this.user.id, this.user.type, this.user.name, null, form.value.userData.eMail,
      form.value.userData.oib, form.value.userData.address, form.value.userData.phoneNumber, this.user.registrationDate,
      this.user.userStatus, this.user.roles, form.value.companyName, form.value.description, form.value.logoName,
      form.value.CV, this.user.jobType);
    this.userService.editUser(this.user).subscribe(
      data => this.router.navigate(['../'], {relativeTo: this.activatedRoute}),
      err => console.log(err)
    );
  }
}
