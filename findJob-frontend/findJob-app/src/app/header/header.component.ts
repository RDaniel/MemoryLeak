import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: User;
  isAdmin = false;

  constructor(private router: Router, public authService: AuthService, private userService: UserService) {
    this.authService.userLoggedIn.subscribe(msg => this.refreshCurrentUser());
   }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.refreshCurrentUser();
    }
  }

  logout() {
    this.authService.logout();
  }

  refreshCurrentUser() {
    this.userService.getCurrentUser().subscribe(
      (user: User) => {
        this.currentUser = user;
        this.isAdmin = user.roles.includes('ADMIN');
      }
    );
  }

  myPage() {
    this.router.navigate(['/korisnik/' + this.currentUser.id]);
  }
}
