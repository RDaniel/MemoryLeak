import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Job } from '../models/job.model';
import { AdvertisementService } from '../services/advertisement.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Jobtype } from '../models/jobtype.model';
import { JobtypeService } from '../services/jobtype.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {
  jobs: Job[] = [];
  keyword: string;
  category: number;
  currentPage: number;
  adsPerPage: number;
  totalPages: number;
  adType: string;
  isLast: boolean;
  isFirst: boolean;
  jobTypes: Jobtype[];

  constructor(private router: Router, private route: ActivatedRoute, private adService: AdvertisementService,
    private jobTypeService: JobtypeService) { }

  ngOnInit(): void {
    this.jobTypeService.getAllJobtypes().subscribe(
      (data: any[]) => {
        this.jobTypes = data;
      }
    );
    this.route.queryParams
      .subscribe(params => {
        // slucaj za kad se stisne /poslovi da se osvjezi na prvu stranicu
        if (!params.stranica) {
          this.currentPage = 0;
          this.adsPerPage = 9;
        } else { // znaci ima parametara
          this.currentPage = params.stranica;
          this.adsPerPage = params.oglasaPoStranici;
          this.keyword = params.kljucnaRijec;
          this.category = params.kategorija;
          this.adType = params.tipOglasa;
        }
        this.adService.getJobs(this.currentPage, this.adsPerPage, this.keyword, this.category, this.adType).subscribe(
          (data: any) => {
            this.jobs = data.content;
            this.totalPages = data.totalPages;
            this.isLast = data.last;
            this.isFirst = data.first;
          }
        );
      });
  }
  onSubmit(form: NgForm) {
    if (form.value.keyword === '') {
      this.keyword = undefined;
    } else {
      this.keyword = form.value.keyword;
    }
    if (form.value.category) {
      if (form.value.category === 'null') {
        this.category = null;
      } else {
        this.category = form.value.category.id;
      }
    } else {
      this.category = null;
    }
    if (form.value.adType === 'null') {
      this.adType = null;
    } else {
      this.adType = form.value.adType;
    }
    this.router.navigate(['/poslovi'], {
      queryParams: {
        stranica: 0, oglasaPoStranici: this.adsPerPage,
        kljucnaRijec: this.keyword, kategorija: this.category, tipOglasa: this.adType
      }
    });
  }

  onPrev() {
    if (this.currentPage >= 1) {
      this.router.navigate(['/poslovi'], {
        queryParams: {
          stranica: + this.currentPage - 1, oglasaPoStranici: this.adsPerPage,
          kljucnaRijec: this.keyword, kategorija: this.category, tipOglasa: this.adType
        }
      });
    }
  }

  onNext() {
    if (this.currentPage < this.totalPages - 1) {
      this.router.navigate(['/poslovi'], {
        queryParams: {
          stranica: +this.currentPage + 1, oglasaPoStranici: this.adsPerPage,
          kljucnaRijec: this.keyword, kategorija: this.category, tipOglasa: this.adType
        }
      });
    }
  }

}
