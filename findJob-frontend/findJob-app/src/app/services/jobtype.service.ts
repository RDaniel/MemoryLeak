import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Jobtype} from '../models/jobtype.model';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class JobtypeService {
    constructor(private http: HttpClient) {}

    getAllJobtypes() {
        return this.http.get(environment.apiUrl + 'registry/jobTypes');
    }

    deleteJobtype( jobtype: string): Observable<any> {
        return this.http.delete(environment.apiUrl + 'registry/jobTypes?name=' + jobtype,
         {responseType: 'text' }).map( response => response);
    }

    addJobtype( newJobtype: string) {
        const newJobType = new Jobtype(newJobtype, null);
        return this.http.post(environment.apiUrl + 'registry/jobTypes', newJobType,
         {responseType: 'text' }).map( response => response);

    }

}
