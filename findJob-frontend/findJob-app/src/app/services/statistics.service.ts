import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Injectable()
export class StatisticsService {
  constructor(private http: HttpClient) {}

  getPageStatistics () {
      return this.http.get(environment.apiUrl + 'statistic/page');
  }

}
