import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { User } from '../models/user.model';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
  url: string;

  constructor(private http: HttpClient) { }

  registerUser(user: User) {
    return this.http.post(environment.apiUrl + 'user/new', user);
  }

  editUser(user: User) {
    return this.http.put(environment.apiUrl + 'user/update', user);
  }

  getByUsername(username: string) {
    return this.http.get(environment.apiUrl + 'user/name?username=' + username, { responseType: 'text' });
  }

  getCurrentUser() {
    return this.http.get(environment.apiUrl + 'user/current');
  }

  getUsers() {
    return this.http.get(environment.apiUrl + 'user/all');
  }

  getUserById(id: number) {
    return this.http.get(environment.apiUrl + 'user/' + id);
  }

  getUsersAsAdmin(pageNumber: number, pageSize: number, keyword: string) {
    this.url = environment.apiUrl + 'user/search?pageNumber=' + pageNumber + '&pageSize=' +
      pageSize;
    if (keyword) {
      this.url = this.url + '&keyWord=' + keyword;
    }
    return this.http.get(this.url);
  }

  deleteUser(id: number) {
    return this.http.delete(environment.apiUrl + 'user/' + id,
      { responseType: 'text' }).map(response => response);
  }

  suspendUser(id: number, banReason: string, expireDate: Date ) {
    console.log(banReason);
    console.log(expireDate);
    return this.http.post(environment.apiUrl + 'user/ban',
      {user: id, banReason: banReason, expirationDate: expireDate},
      { responseType: 'text' }).map(response => response);
  }

  removeSuspend(id: number) {
    return this.http.delete(environment.apiUrl + 'user/ban/' + id,
    { responseType: 'text' }).map(response => response);
  }
}
