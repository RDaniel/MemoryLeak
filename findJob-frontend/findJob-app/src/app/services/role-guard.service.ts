import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { User } from '../models/user.model';

@Injectable()
export class RoleGuardService implements CanActivate {
  isAdmin: string;

  constructor(public auth: AuthService, public router: Router, public user: UserService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    this.isAdmin = this.auth.isAdmin();
    if (this.isAdmin === 'true') {
      return true;
    } else {
      alert('Nemate ovlasti za pristupiti ovoj stranici');
      this.router.navigate(['/poslovi']);
      return false;
    }
  }
}
