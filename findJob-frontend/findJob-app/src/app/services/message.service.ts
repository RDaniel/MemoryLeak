import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Message } from '../models/message.model';
import { environment } from '../../environments/environment';

@Injectable()
export class MessageService {
  url: string;

  constructor(private http: HttpClient) { }

  sendMessage(message: Message) {
    return this.http.post(environment.apiUrl + 'conversation/message/new', message);
  }
}
