import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Conversation } from '../models/conversation.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ConversationService {
  constructor(private http: HttpClient) { }

  getConversations(userId: number) {
    return this.http.get(environment.apiUrl + 'conversation/all?id=' + userId);
  }
  getConversationMessages(conversationId: number) {
    return this.http.get(environment.apiUrl + 'conversation/message/' + conversationId);
  }
  newConversation(conversation: Conversation) {
    return this.http.post(environment.apiUrl + 'conversation/new', conversation);
  }
  getConversation(conversationId: number) {
    return this.http.get(environment.apiUrl + 'conversation/' + conversationId);
  }
  getConversationByAdId(adId: number) {
    return this.http.get(environment.apiUrl + 'conversation/ad/current/' + adId);
  }
}
