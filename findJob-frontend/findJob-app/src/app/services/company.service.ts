import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Injectable()
export class CompanyService {
  url: string;
  constructor(private http: Http) {}

  getCompanies (currentPage: number, adsPerPage: number, keyword: string, category: string, rating: number) {
    this.url = environment.apiUrl + 'user/company/all?pageNumber=' + currentPage + '&pageSize=' +
    adsPerPage;
    if (keyword) {
      this.url = this.url + '&keyWord=' + keyword;
    }
    if (category) {
      this.url = this.url + '&category=' + category;
    }
    if (rating) {
      this.url = this.url + '&minGrade=' + rating;
    }
    return this.http.get(this.url)
    .map(
      (response: Response) => {
        const data = response.json();
        return data;
      }
    );
  }
  getCompany(id: number) {
    return this.http.get(environment.apiUrl + 'user/' + id)
    .map(
      (response: Response) => {
        const data = response.json();
        return data;
      }
    );
  }

}
