import * as moment from 'moment';
import { Injectable, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';
import { UserService } from './user.service';
import { User } from '../models/user.model';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
    @Output() userLoggedIn: EventEmitter<any> = new EventEmitter();
    constructor(private http: HttpClient, private router: Router, private userService: UserService) {}

    login(username: string, password: string ) {
        return this.http.post( environment.login + 'login', { username: username, password: password },
        { observe: 'response', responseType: 'text' })
        .subscribe(
                (response) => this.setSession(response.headers.get('Authorization')),
                error => alert('Pogrešno korisničko ime ili lozinka')
            );
    }

    private setSession(authResult) {
        const token = authResult.split(' ')[1];
        const expiresAt = authResult.split(' ')[0];

        localStorage.setItem('id_token', token);
        localStorage.setItem('expires_at', JSON.stringify(expiresAt));
        this.userLoggedIn.emit('novi user');
        this.router.navigate(['/poslovi']);

        this.userService.getCurrentUser().subscribe(
            (user: User) => {
                localStorage.setItem('admin', user.roles.includes('ADMIN') + '');
            },
            error => localStorage.setItem('admin', 'false')
        );
    }

    logout() {
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        localStorage.removeItem('admin');
        this.router.navigate(['/poslovi']);
    }

    public isLoggedIn() {
        return moment().valueOf() < this.getExpiration();
    }

    getExpiration() {
        const expiration = localStorage.getItem('expires_at');
        return JSON.parse(expiration);
    }

    public isAdmin() {
        if (localStorage.getItem('admin')) {
            return localStorage.getItem('admin');
        }
        return 'false';
    }
}
