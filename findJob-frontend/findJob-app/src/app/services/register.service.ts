import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Person } from '../models/person.model';
import { Company } from '../models/company.model';
import { User } from '../models/user.model';
import { environment } from '../../environments/environment';

@Injectable()
export class RegisterService {
    constructor(private http: HttpClient) { }

    register(user: User) {
        return this.http.post(environment.apiUrl + 'user/new', user);
    }

}
