import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Company } from '../models/company.model';
import { User } from '../models/user.model';
import { Companyreview } from '../models/companyreview.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ReviewsService {
  constructor(private http: HttpClient) {}

  getAllReviews () {
      return this.http.get(environment.apiUrl + 'user/company/review/all');
  }

  deleteReview(id: number) {
    return this.http.delete(environment.apiUrl + 'user/company/review/' + id,
    {responseType: 'text' }).map( response => response);
  }

  addReview( company: Company, author: User, grade: number, description: string) {
    // const review = new Companyreview(null, company, author, grade, description);
    return this.http.post(environment.apiUrl + 'user/company/review/new',
     {company: company.id, author: author.id, grade: grade, description: description},
    {responseType: 'text' }).map( response => response);
  }

}
