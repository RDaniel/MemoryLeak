import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Job } from '../models/job.model';
import { Jobtype } from '../models/jobtype.model';
import { environment } from '../../environments/environment';

@Injectable()
export class AdvertisementService {
  url: string;
  constructor(private http: HttpClient) {}

  getJobs(currentPage: number, adsPerPage: number, keyword: string, category: number, adType: string) {
    this.url = environment.apiUrl + 'advertisement/all?pageNumber=' + currentPage + '&pageSize=' +
    adsPerPage;
    if (keyword) {
      this.url = this.url + '&keyWord=' + keyword;
    }
    if (category) {
      this.url = this.url + '&category=' + category;
    }
    if (adType) {
      this.url = this.url + '&adType=' + adType;
    }
    return this.http.get(this.url);
  }

  getJob(id: number) {
    return this.http.get(environment.apiUrl + 'advertisement/' + id);
  }

  getAllJobsForUser(ownerId: number) {
    return this.http.get(environment.apiUrl + 'advertisement/owner?id=' + ownerId);
  }

  deleteJob (id: number) {
    console.log(id);
    return this.http.delete(environment.apiUrl + 'advertisement/' + id,
    {responseType: 'text' }).map( response => response);
  }

  createJob(job: Job) {
    return this.http.post(environment.apiUrl + 'advertisement/new', job, {
      responseType: 'text'
    });
  }
}
