import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { JobsComponent } from './jobs/jobs.component';
import { JobItemComponent } from './job-item/job-item.component';
import { UserComponent } from './user/user.component';
import { PersonalInfoComponent } from './user/personal-info/personal-info.component';
import { MessagesComponent } from './user/messages/messages.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { CompaniesComponent } from './companies/companies.component';
import { AdvertisementService } from './services/advertisement.service';
import { HttpModule } from '@angular/http';
import { CompanyService } from './services/company.service';
import { AdminDashboardComponent } from './admin-page/admin-dashboard/admin-dashboard.component';
import { AdminRecensionsComponent } from './admin-page/admin-recensions/admin-recensions.component';
import { AdminUsersComponent } from './admin-page/admin-users/admin-users.component';
import { AdminAdvertisementComponent } from './admin-page/admin-advertisement/admin-advertisement.component';
import { RegisterPersonComponent } from './register-person/register-person.component';
import { RegisterFirmComponent } from './register-firm/register-firm.component';
import { LoginComponent } from './login/login.component';
import { AdvertCreateComponent } from './advert-create/advert-create.component';
import { AuthService } from './services/auth.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/auth.interceptor';
import { AuthGuardService } from './services/auth-guard.service';
import { UserService } from './services/user.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { RoleGuardService } from './services/role-guard.service';
import { AdminJobtypesComponent } from './admin-page/admin-jobtypes/admin-jobtypes.component';
import { JobtypeService} from './services/jobtype.service';
import { EditFirmComponent } from './edit-firm/edit-firm.component';
import { EditPersonComponent } from './edit-person/edit-person.component';
import { AppRoutingModule } from './app-routing.module';
import { RegisterService } from './services/register.service';
import { MyAdvertisementsComponent } from './user/my-advertisements/my-advertisements.component';
import { ConversationService } from './services/conversation.service';
import { ConversationComponent } from './user/conversation/conversation.component';
import { StatisticsService } from './services/statistics.service';
import { ReviewsService } from './services/reviews.service';
import { NewConversationComponent } from './user/new-conversation/new-conversation.component';
import { MessageService } from './services/message.service';
import { ReversePipe } from './pipe/pipe';
import {Ng2CompleterModule} from 'ng2-completer';


@NgModule({
  declarations: [
    AppComponent,
    ReversePipe,
    HeaderComponent,
    JobsComponent,
    JobItemComponent,
    UserComponent,
    PersonalInfoComponent,
    MessagesComponent,
    CompanyDetailsComponent,
    AdminPageComponent,
    CompaniesComponent,
    AdminDashboardComponent,
    AdminRecensionsComponent,
    AdminUsersComponent,
    AdminAdvertisementComponent,
    RegisterPersonComponent,
    RegisterFirmComponent,
    LoginComponent,
    AdvertCreateComponent,
    AdminJobtypesComponent,
    EditFirmComponent,
    EditPersonComponent,
    MyAdvertisementsComponent,
    ConversationComponent,
    NewConversationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    Ng2CompleterModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    AdvertisementService,
    CompanyService,
    JobtypeService,
    AuthService,
    AuthGuardService,
    UserService,
    HttpClient,
    HttpClientModule,
    RoleGuardService,
    RegisterService,
    ConversationService,
    MessageService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    StatisticsService,
    ReviewsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
