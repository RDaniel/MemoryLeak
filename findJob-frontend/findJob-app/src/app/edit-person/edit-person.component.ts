import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Person } from '../models/person.model';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.css']
})
export class EditPersonComponent implements OnInit {
  user: Person;

  constructor(private userService: UserService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(
      (user: Person) => {
        this.user = user;
      }
    );
  }

  onSubmit(form: NgForm) {
    if (form.value.userData.eMail === '') {form.value.userData.eMail = this.user.eMail; }
    if (form.value.userData.oib === '') {form.value.userData.oib = this.user.oib; }
    if (form.value.userData.address === '') {form.value.userData.address = this.user.address; }
    if (form.value.userData.phoneNumber === '') {form.value.userData.phoneNumber = this.user.phoneNumber; }
    if (form.value.firstName === '') {form.value.firstName = this.user.firstName; }
    if (form.value.lastName === '') {form.value.lastName = this.user.lastName; }
    if (form.value.dateOfBirth === '') {form.value.dateOfBirth = this.user.dateOfBirth; }
    if (form.value.cv === '') {form.value.cv = this.user.cv; }
    console.log('Person edit has been submitted');
    this.user = new Person(this.user.id, this.user.type, this.user.name, null, form.value.userData.eMail,
      form.value.userData.oib, form.value.userData.address, form.value.userData.phoneNumber, this.user.registrationDate,
      this.user.userStatus, this.user.roles, form.value.firstName, form.value.lastName, form.value.dateOfBirth,
      form.value.cv, this.user.areaOfInterest);
    this.userService.editUser(this.user).subscribe(
      data => this.router.navigate(['../'], {relativeTo: this.activatedRoute}),
      err => console.log(err)
    );
  }
}
