import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Company } from '../models/company.model';
import { CompanyService } from '../services/company.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Jobtype } from '../models/jobtype.model';
import { JobtypeService } from '../services/jobtype.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {
  companies: Company[] = [];
  keyword: string;
  category: string;
  rating: number;
  currentPage: number;
  adsPerPage: number;
  totalPages: number;
  isLast: boolean;
  isFirst: boolean;
  jobTypes: Jobtype[];

  constructor(private router: Router, private route: ActivatedRoute, private compService: CompanyService,
    private jobTypeService: JobtypeService) { }

  ngOnInit() {
    this.jobTypeService.getAllJobtypes().subscribe(
      (data: any[]) => {
        this.jobTypes = data;
      }
    );
    this.route.queryParams
      .subscribe(params => {
        // slucaj za kad se stisne /poslovi da se osvjezi na prvu stranicu
        if (!params.stranica) {
          this.currentPage = 0;
          this.adsPerPage = 9;
        } else { // znaci ima parametara
          this.currentPage = params.stranica;
          this.adsPerPage = params.oglasaPoStranici;
          this.keyword = params.kljucnaRijec;
          this.category = params.kategorija;
          this.rating = params.ocjena;
        }
        this.compService.getCompanies(this.currentPage, this.adsPerPage, this.keyword, this.category, this.rating).subscribe(
          (data: any) => {
            this.companies = data.content;
            this.totalPages = data.totalPages;
            this.isLast = data.last;
            this.isFirst = data.first;
          }
        );
      });
  }

  onSubmit(form: NgForm) {
    if (form.value.keyword === '') {
      this.keyword = undefined;
    } else {
      this.keyword = form.value.keyword;
    }
    if (form.value.category) {
      if (form.value.category === 'null') {
        this.category = null;
      } else {
        this.category = form.value.category.id;
      }
    } else {
      this.category = null;
    }
    this.rating = form.value.rating;
    if (form.value.rating === '0') { this.rating = undefined; }
    this.router.navigate(['/tvrtke'], {
      queryParams: {
        stranica: 0, oglasaPoStranici: this.adsPerPage,
        kljucnaRijec: this.keyword, kategorija: this.category, ocjena: this.rating
      }
    });
  }

  onPrev() {
    if (this.currentPage >= 1) {
      this.router.navigate(['/tvrtke'], {
        queryParams: {
          stranica: +this.currentPage - 1, oglasaPoStranici: this.adsPerPage,
          kljucnaRijec: this.keyword, kategorija: this.category, ocjena: this.rating
        }
      });
    }
  }

  onNext() {
    if (this.currentPage < this.totalPages - 1) {
      this.router.navigate(['/tvrtke'], {
        queryParams: {
          stranica: +this.currentPage + 1, oglasaPoStranici: this.adsPerPage,
          kljucnaRijec: this.keyword, kategorija: this.category, ocjena: this.rating
        }
      });
    }
  }
}
