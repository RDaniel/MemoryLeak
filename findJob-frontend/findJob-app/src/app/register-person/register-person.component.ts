import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Person } from '../models/person.model';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { Jobtype } from '../models/jobtype.model';
import { UserService } from '../services/user.service';
import { JobtypeService } from '../services/jobtype.service';

@Component({
  selector: 'app-register-person',
  templateUrl: './register-person.component.html',
  styleUrls: ['./register-person.component.css']
})
export class RegisterPersonComponent implements OnInit {
  user: User;
  jobTypes: Jobtype[];

  constructor(private userService: UserService, private authService: AuthService,
    private jobTypeService: JobtypeService) { }

  ngOnInit() {
    this.jobTypeService.getAllJobtypes().subscribe(
      (data: any[]) => {
      this.jobTypes = data;
      }
    );
  }

  onSubmit(form: NgForm) {
    this.userService.getByUsername(form.value.userData.username).subscribe(
      (user: any) => {
        if (user === null) {
          this.register(form);
        } else {
          alert('Odabrano korisničko ime već postoji.');
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  register(form: NgForm) {
    if (form.value.userData.oib === '') { form.value.userData.oib = null; }
    if (form.value.cv === '') { form.value.cv = null; }
    if (form.value.userData.address === '') { form.value.userData.address = null; }
    if (form.value.userData.phoneNumber === '') { form.value.userData.phoneNumber = null; }
    if (form.value.dateOfBirth === '') { form.value.dateOfBirth = null; }
    if (form.value.category === 'null') { form.value.category = null; }
    console.log('New person has been submitted');
    this.user = new Person(null, 'person', form.value.userData.username, form.value.userData.password,
      form.value.userData.email, form.value.userData.oib, form.value.userData.address, form.value.userData.phoneNumber,
      null, null, ['PERSON'], form.value.firstName, form.value.lastName, form.value.dateOfBirth,
      form.value.cv, form.value.category);
    this.userService.registerUser(this.user).subscribe(
      data => this.authService.login(form.value.userData.username, form.value.userData.password),
      err => console.log(err)
    );
  }
}
