import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Conversation } from '../../models/conversation.model';
import { ConversationService } from '../../services/conversation.service';
import { Message } from '../../models/message.model';
import { Job } from '../../models/job.model';
import { AdvertisementService } from '../../services/advertisement.service';
import { MessageService } from '../../services/message.service';
import { CompleterData, CompleterItem, CompleterService } from 'ng2-completer';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-new-conversation',
  templateUrl: './new-conversation.component.html',
  styleUrls: ['./new-conversation.component.css']
})
export class NewConversationComponent implements OnInit {
  private static readonly DISPLAY_FIELD = 'name,eMail';
  private static readonly SEARCH_URL = environment.apiUrl + 'user/get?keyWord=';

  warning: string;
  haveReceiver: boolean;
  receiver: User;
  conversation: Conversation;
  sender: User;
  message: Message;
  advertisement: Job;
  userString: string;
  receiverUsername: string;

  completerItems: CompleterItem[];
  dataService: CompleterData;

  constructor(private userService: UserService, private route: ActivatedRoute,
    private convService: ConversationService, private completerService: CompleterService,
    private adService: AdvertisementService, private router: Router,
    private messageService: MessageService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.dataService = this.completerService
      .remote(NewConversationComponent.SEARCH_URL, null, NewConversationComponent.DISPLAY_FIELD);
    this.dataService.subscribe(result => {
      this.completerItems = result;
    });

    this.warning = 'Korisnik nije pronadjen';
    // provjeri postoje li queryparametars
    this.route.queryParams.subscribe(params => {
      if (params.primatelj) {
        this.getReceiver(params.primatelj);
        this.haveReceiver = true;
        this.getAdvertisement(params.oglas);
      } else {
        this.haveReceiver = false;
      }
    }
    );
    this.userService.getCurrentUser().subscribe(
      (user: User) => this.sender = user
    );
  }

  getAdvertisement(adId: number) {
    this.adService.getJob(adId).subscribe(
      (job: Job) => {
        this.advertisement = job;
      }
    );
  }

  getReceiver(id: number) {
    this.userService.getUserById(id).subscribe(
      (user: User) => this.receiver = user
    );
  }

  onSubmit(form: NgForm) {
    if (this.haveReceiver) {
      this.newConversation(form);
    } else {
      this.receiverUsername = form.value.user.split(' ')[0];
      this.userService.getByUsername(this.receiverUsername).subscribe(
        (user: any) => {
          this.receiver = JSON.parse(user);
          this.newConversation(form);
        }
      );
    }
  }

  newConversation(form: NgForm) {
    this.conversation = new Conversation(0, [this.receiver, this.sender], [], this.advertisement);
    this.convService.newConversation(this.conversation).subscribe(
      (data: Conversation) => {
        this.conversation = data;
        this.newMessage(form.value.message);
        this.router.navigate(['../'], { relativeTo: this.activatedRoute });
      }
    );
  }

  newMessage(message: string) {
    this.message = new Message(null, null, this.receiver, message, null, this.conversation);
    this.messageService.sendMessage(this.message).subscribe(
      (data) => console.log(data)
    );
  }

}
