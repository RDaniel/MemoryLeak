import { Component, OnInit } from '@angular/core';
import { Conversation } from '../../models/conversation.model';
import { UserService } from '../../services/user.service';
import { ConversationService } from '../../services/conversation.service';
import { ActivatedRoute } from '@angular/router/';
import { Router } from '@angular/router/';
import { User } from '../../models/user.model';
import { ReversePipe } from '../../pipe/pipe';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  warning: string;
  conversations: Conversation[];
  currentUsername: string;
  constructor(private router: Router, private userService: UserService,
    private convoService: ConversationService, private activatedRoute: ActivatedRoute) {  }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(
      (user: any) => {
        this.getConversations(user.id);
        this.currentUsername = user.name;
      }
    );
  }

  getConversations(userId: number) {
    this.convoService.getConversations(userId).subscribe(
      (conversations: any[]) => {
        this.conversations = conversations;
        if (this.conversations.length === 0) {
          this.warning = 'Nema razgovora';
        }
      }
    );
  }

  openConversation(conversationId: number) {
    this.router.navigate(['../razgovori/' + conversationId], {relativeTo: this.activatedRoute});
  }

  newConversation() {
    this.router.navigate(['../razgovori/novi'], {relativeTo: this.activatedRoute});
  }

}
