import { Component, OnInit } from '@angular/core';
import { Job } from '../../models/job.model';
import { AdvertisementService } from '../../services/advertisement.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-my-advertisements',
  templateUrl: './my-advertisements.component.html',
  styleUrls: ['./my-advertisements.component.css']
})

export class MyAdvertisementsComponent implements OnInit {
  jobs: Job[] = [];
  warning: string;
  constructor(private adService: AdvertisementService, private userService: UserService) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(
      (user: any) => {
        this.getAllJobs(user.id);
      }
    );
  }

  getAllJobs(currentUserId) {
    this.adService.getAllJobsForUser(currentUserId).subscribe(
      (jobs: any[]) => {
        this.jobs = jobs;
        if (this.jobs.length === 0) {
          this.warning = 'Nema oglasa';
        }
      }
    );
  }
}
