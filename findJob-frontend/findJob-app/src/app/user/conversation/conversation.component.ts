import { Component, OnInit, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router/';
import { ConversationService } from '../../services/conversation.service';
import { Message } from '../../models/message.model';
import { NgForm } from '@angular/forms';
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { Conversation } from '../../models/conversation.model';
import { ReversePipe } from '../../pipe/pipe';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})

@Injectable()
export class ConversationComponent implements OnInit {

  conversation: Conversation;
  newMessage: Message;
  date: string;
  currentName;

  constructor(private activatedRoute: ActivatedRoute,
    private conversationService: ConversationService, private messageService: MessageService,
  private userservice: UserService) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(params => {
        this.getConversation(params.id);
      });
  }

  getConversation(coversationId: number) {
    this.conversationService.getConversation(coversationId).subscribe(
      (conversation: Conversation) => {
        this.conversation = conversation;
        this.formatDate();
      },
      err => console.log(err)
    );
  }
  formatDate() {
    this.userservice.getCurrentUser().subscribe(
      (user: User) => {
        this.currentName = user.name;
      }
    );
  }

  onSubmit(form: NgForm) {
    let reciever = null;
    if (this.conversation.participants[0] === this.currentName) {
      reciever = this.conversation.participants[1];
    } else {
      reciever = this.conversation.participants[0];
    }
    this.newMessage = new Message(null, null, reciever, form.value.message, null, this.conversation);

    this.messageService.sendMessage(this.newMessage).subscribe(
      (data: Message) => {
        this.conversation.messages.unshift(data);
      }
    );
    form.reset();
  }

}
