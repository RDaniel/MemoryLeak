import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { Router } from '@angular/router/';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css']
})
export class PersonalInfoComponent implements OnInit {
  user: User;
  person: boolean;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(
      (user: User) => {
        this.user = user;
        if (user.type === 'person') {
          this.person = true;
        } else {
          this.person = false;
        }
      }
    );
  }

  editUser() {
    if (this.person) {
      this.router.navigate(['korisnik/' + this.user.id + '/podaci/uredi-osobu']);
    } else {
      this.router.navigate(['/korisnik/' + this.user.id + '/podaci/uredi-tvrtku']);
    }
  }

}
