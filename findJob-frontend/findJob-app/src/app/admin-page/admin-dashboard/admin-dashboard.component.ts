import { Component, OnInit } from '@angular/core';
import {StatisticsService } from '../../services/statistics.service';
import {Statistics} from '../../models/statistics.model';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  pageStats: Statistics;

  constructor(private statService: StatisticsService) { }

  ngOnInit() {

    this.statService.getPageStatistics().subscribe(
      (stats: any) => {
        this.pageStats = stats;
      }
    );
  }

}
