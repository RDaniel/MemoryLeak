import { Component, OnInit } from '@angular/core';
import { Companyreview } from '../../models/companyreview.model';
import { ReviewsService } from '../../services/reviews.service';

@Component({
  selector: 'app-admin-recensions',
  templateUrl: './admin-recensions.component.html',
  styleUrls: ['./admin-recensions.component.css']
})

export class AdminRecensionsComponent implements OnInit {

  reviews: Companyreview[];
  currentPage: number;
  itemsPerPage: number;
  totalPages: number;
  isLast: boolean;
  isFirst: boolean;

  constructor(private reviewService: ReviewsService) { }

  ngOnInit() {
    this.currentPage = 0;
    this.fetchData();
  }

  fetchData() {
    this.reviewService.getAllReviews().subscribe(
      (data: any) => {
        this.reviews = data.content;
        this.totalPages = data.totalPages;
        this.isFirst = data.isFirst;
        this.isLast = data.isLast;
      }
    );
  }

  onPrev() {
    if (this.currentPage >= 1) {
      this.currentPage--;
      this.fetchData();
    }
  }

  onNext() {
    if (this.currentPage < this.totalPages - 1) {
      this.currentPage++;
      this.fetchData();
    }
  }

  deleteReview(id: number) {
    let self = this;
    this.reviewService.deleteReview(id).subscribe(
      resp => { self.fetchData(); }
    );
  }

}
