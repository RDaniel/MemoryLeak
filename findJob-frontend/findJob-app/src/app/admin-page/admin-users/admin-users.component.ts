import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

  users: User[];
  keyword: string;
  currentPage: number;
  itemsPerPage: number;
  totalPages: number;
  isLast: boolean;
  isFirst: boolean;
  userSuspending: User = undefined;


  constructor (private router: Router, private route: ActivatedRoute, private userService: UserService) {}

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        // slucaj za kad se stisne /poslovi da se osvjezi na prvu stranicu
        if (!params.pageNumber) {
          this.currentPage = 0;
          this.itemsPerPage = 9;
        } else { // znaci ima parametara
          this.currentPage = params.pageNumber;
          this.itemsPerPage = params.itemsPerPage;
          this.keyword = params.keyword;
        }
        this.fetchData();
      });
  }

  fetchData() {

    this.userService.getUsersAsAdmin(this.currentPage, this.itemsPerPage, this.keyword).subscribe (
      (data: any) => {
          this.users = data.content;
          this.totalPages = data.totalPages;
          this.isLast = data.last;
          this.isFirst = data.first;
        }
    );

  }
  onSubmit(form: NgForm) {
   this.keyword = form.value.keyword;
    if (form.value.keyword === '') {this.keyword = undefined; }
    console.log(this.keyword);
    this.router.navigate(['/administrator/admin-korisnici'], { queryParams: { pageNumber: 0, itemsPerPage: this.itemsPerPage,
      keyword: this.keyword} });
  }

  onPrev() {
    if (this.currentPage >= 1) {
      this.router.navigate(['/administrator/admin-korisnici'],
      { queryParams: { pageNumber: +this.currentPage - 1, itemsPerPage: this.itemsPerPage,
        keyword: this.keyword} });
    }
  }

  onNext() {
    if (this.currentPage < this.totalPages - 1) {
      this.router.navigate(['/administrator/admin-korisnici'],
      { queryParams: { pageNumber: +this.currentPage + 1, itemsPerPage: this.itemsPerPage,
        keyword: this.keyword} });
    }
  }

  deleteUser(id: number) {
    this.userService.deleteUser(id).subscribe(
      resp => { this.fetchData(); }
    );
  }

  onSuspend(user: User) {
    this.userSuspending = user;
  }

  cancelSuspend() {
    this.userSuspending = undefined;
  }

  suspendUser(form: NgForm) {
    this.userService.suspendUser(this.userSuspending.id, form.value.description, form.value.expDate).subscribe(
       resp => { this.fetchData(); }
    );
    this.cancelSuspend();
  }

  removeSuspend(id: number) {
    this.userService.removeSuspend(id).subscribe(
      resp => { this.fetchData(); }
   );
  }

}
