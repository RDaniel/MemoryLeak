import { Component, OnInit } from '@angular/core';
import { JobtypeService } from '../../services/jobtype.service';
import { Jobtype } from '../../models/jobtype.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-admin-jobtypes',
  templateUrl: './admin-jobtypes.component.html',
  styleUrls: ['./admin-jobtypes.component.css']
})
export class AdminJobtypesComponent implements OnInit {

  jobtypes: Jobtype[];

  constructor( private jobtypeService: JobtypeService) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.jobtypeService.getAllJobtypes().subscribe(
        (jobtype: any[]) => {
          this.jobtypes = jobtype;
        }
    );
  }

  deleteJobtype(jobtype: string) {
    let self = this;
    this.jobtypeService.deleteJobtype(jobtype).subscribe(
      resp => { self.fetchData(); }
    );
  }

  addJobtype(newJobtype: string) {
    let self = this;
    if (newJobtype !== '' && newJobtype !== undefined) {
      console.log('predan ' + newJobtype);
      this.jobtypeService.addJobtype(newJobtype).subscribe(
        resp => { self.fetchData(); }
      );
    }
  }
}
