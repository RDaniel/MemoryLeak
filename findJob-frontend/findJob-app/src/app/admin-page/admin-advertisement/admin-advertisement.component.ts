import { Component, OnInit } from '@angular/core';
import { Job } from '../../models/job.model';
import { AdvertisementService } from '../../services/advertisement.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-admin-advertisement',
  templateUrl: './admin-advertisement.component.html',
  styleUrls: ['./admin-advertisement.component.css']
})
export class AdminAdvertisementComponent implements OnInit {

  jobs: Job[] = [];
  keyword: string;
  currentPage: number;
  adsPerPage: number;
  totalPages: number;
  adType: string;
  isLast: boolean;
  isFirst: boolean;


  constructor (private router: Router, private route: ActivatedRoute, private adService: AdvertisementService) {}

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        // slucaj za kad se stisne /poslovi da se osvjezi na prvu stranicu
        if (!params.stranica) {
          this.currentPage = 0;
          this.adsPerPage = 9;
        } else { // znaci ima parametara
          this.currentPage = params.stranica;
          this.adsPerPage = params.oglasaPoStranici;
          this.keyword = params.kljucnaRijec;
          this.adType = params.tipOglasa;
        }
        this.fetchData();
      });
  }

  fetchData() {
    this.adService.getJobs(this.currentPage, this.adsPerPage, this.keyword, null, this.adType).subscribe (
      (data: any) => {
          this.jobs = data.content;
          this.totalPages = data.totalPages;
          this.isLast = data.last;
          this.isFirst = data.first;
        }
    );
  }

  onSubmit(form: NgForm) {
    console.log(form.value);
    this.keyword = form.value.keyword;
    this.adType = form.value.adType;
    if (form.value.adType === 'SVI') {this.adType = undefined; }
    if (form.value.keyword === '') {this.keyword = undefined; }
    this.router.navigate(['/administrator/admin-oglasi'], { queryParams: { stranica: 0, oglasaPoStranici: this.adsPerPage,
      kljucnaRijec: this.keyword, tipOglasa: this.adType } });
  }

  onPrev() {
    if (this.currentPage >= 1) {
      this.router.navigate(['/administrator/admin-oglasi'],
      { queryParams: { stranica: +this.currentPage - 1, oglasaPoStranici: this.adsPerPage,
      kljucnaRijec: this.keyword, tipOglasa: this.adType} });
    }
  }

  onNext() {
    console.log('clicked1');
    if (this.currentPage < this.totalPages - 1) {
      console.log('clicked2');
      this.router.navigate(['/administrator/admin-oglasi'],
      { queryParams: {stranica: +this.currentPage + 1, oglasaPoStranici: this.adsPerPage,
        kljucnaRijec: this.keyword, tipOglasa: this.adType } });
    }
  }

    onDelete(id: number) {
      let self = this;
      this.adService.deleteJob(id).subscribe(
        resp => { self.fetchData(); }
      );
  }

}

