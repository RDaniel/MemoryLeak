import { Component, OnInit } from '@angular/core';
import { Company } from '../models/company.model';
import { CompanyService } from '../services/company.service';
import { ActivatedRoute } from '@angular/router';
import { Companyreview } from '../models/companyreview.model';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';
import { ReviewsService } from '../services/reviews.service';
import { UserService } from '../services/user.service';
import { NgForm } from '@angular/forms';
import { ReversePipe } from '../pipe/pipe';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css']
})
export class CompanyDetailsComponent implements OnInit {
  company: Company;
  user: User;
  companyReviews: Companyreview[];
  numberOfReviews: number;
  rateStars: number;
  fullStars: boolean[] = [false , false, false, false, false];
  hoverStars: boolean[] = [false , false, false, false, false];
  numbers: number[] = [0 , 1 , 2 , 3 , 4];

  constructor(private route: ActivatedRoute, private adService: CompanyService,
     public authService: AuthService, private reviewService: ReviewsService, private userService: UserService) { }

  ngOnInit() {
    this.fetchData();
    this.rateStars = 0;
  }

  fetchData() {
    const id = this.route.snapshot.params['id'];
    this.adService.getCompany(id).subscribe(
      (company: any) => {
          this.company = company;
          this.companyReviews = company.companyReviews;
          this.numberOfReviews = Object.keys(company.companyReviews).length;
        }
    );
    this.userService.getCurrentUser().subscribe(
      (user: any) => {
        this.user = user;
      },
      err => console.log('')
    );
  }

  addNewReview(form: NgForm) {
    this.reviewService.addReview(this.company, this.user, this.rateStars, form.value.description).subscribe(
      /*data => console.log('ovo' + data),
      err => console.log(err.message),*/
      resp => { this.fetchData(); }
    );
    form.reset();
    this.rateStars = 0;
    this.resetStars();
    for (let i of this.numbers) {
      this.fullStars[i] = false;
    }
  }

  isNotSameUser() {
    if (this.company && this.user) {
      return this.company.id !== this.user.id;
    }
    return true;
  }

  rate(rating: number) {
    this.rateStars = rating;
    // console.log('Rating: ' + this.rateStars + 'stars!');
    // restart stars
    for (let i of this.numbers) {
      this.fullStars[i] = false;
    }

    // make stars full
    for (let i of this.numbers) {
      if ( i < this.rateStars ) {
        this.fullStars[i] = true;
        // console.log(i.toString() + this.fullStars[i]);
      }
    }
  }

  resetStars() {
    for (let i of this.numbers) {
      this.hoverStars[i] = false;
    }
  }

  setHover(hoverIndex: number) {
    this.resetStars();
    for (let i of this.numbers) {
      if ( i < hoverIndex ) {
        this.hoverStars[i] = true;
      }
    }
  }

}
