import { User } from './user.model';
import { Job } from './job.model';
import { Message } from './message.model';

export class Conversation {
    public id: number;
    public participants: User[];
    public messages: Message[];
    public advertisement: Job;

    constructor(id: number, participants: User[], messages: Message[], advertisement: Job) {
        this.id = id;
        this.participants = participants;
        this.messages = messages;
        this.advertisement = advertisement;
    }
}
