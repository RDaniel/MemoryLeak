export class User {
    public id: number;
    public type: string;
    public name: string;
    public password: string;
    public eMail: string;
    public oib: string;
    public address: string;
    public phoneNumber: string;
    public registrationDate: string;
    public userStatus: string;
    public roles: string[];

    constructor (id: number, type: string, name: string, password: string, eMail: string,
        oib: string, address: string, phoneNumber: string, regDate: string, userStatus: string, role: string[]) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.password = password;
        this.eMail = eMail;
        this.oib = oib;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.registrationDate = regDate;
        this.userStatus = userStatus;
        this.roles = role;
    }
}
