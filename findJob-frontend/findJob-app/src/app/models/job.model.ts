import { User } from './user.model';

export class Job {
    public id: number;
    public name: string;
    public description: string;
    public jobType: string;
    public type: string;
    public expirationDate: Date;
    public imagePath: string;
    public owner: any;

    constructor(id: number, name: string, desc: string, jobType: string, type: string,
        expD: Date, imagePath: string, owner: any) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.jobType = jobType;
        this.type = type;
        this.expirationDate = expD;
        this.imagePath = imagePath;
        this.owner = owner;
    }
}
