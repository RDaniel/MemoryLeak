export class Jobtype {

    public jobType: string;
    public id: number;

    constructor(jobtype: string, id: number) {

        this.id = id;
        this.jobType = jobtype;

    }
}
