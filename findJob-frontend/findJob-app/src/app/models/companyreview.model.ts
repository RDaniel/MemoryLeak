import { Company } from './company.model';
import { User } from './user.model';

export class Companyreview {
    public id: number;
    public company: Company;
    public author: User;
    public grade: number;
    public description: string;

    constructor(id: number, company: Company, author: User, grade: number, description: string) {
     this.id = id;
     this.company = company;
     this.author = author;
     this.grade = grade;
     this.description = description;
    }
}
