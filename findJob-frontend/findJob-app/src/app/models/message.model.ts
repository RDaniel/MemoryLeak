import { User } from './user.model';
import { Conversation } from './conversation.model';
import { send } from 'q';

export class Message {
    public id: number;
    public sender: User;
    public recipient: User;
    public text: string;
    public sendDate: Date;
    public conversation: Conversation;

    constructor(id: number, sender: User, recipient: User, text: string, sendDate: Date, conversation: Conversation) {
        this.id = id;
        this.sender = sender;
        this.recipient = recipient;
        this.text = text;
        this.sendDate = sendDate;
        this.conversation = conversation;
    }
}
