import { User } from './user.model';
import { Jobtype } from './jobtype.model';

export class Person extends User {
    public firstName: string;
    public lastName: string;
    public dateOfBirth: Date;
    public cv: string;
    public areaOfInterest: Jobtype;

    constructor (id: number, type: string, username: string, password: string, eMail: string,
        oib: string, address: string, phoneNum: string, regDate: string, userStatus: string, role: string[],
        firstName: string, lastName: string, dOB: Date, cv: string, aOI: Jobtype) {
        super(id, type, username, password, eMail, oib, address, phoneNum, regDate, userStatus, role);
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dOB;
        this.cv = cv;
        this.areaOfInterest = aOI;
    }
}
