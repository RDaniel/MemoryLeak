export class Statistics {

    public numberOfPersons: number;

    public numberOfCompanies: number;

    public numberOfAdvertisements: number;

    constructor(numberOfPersons: number, numberOfCompanies: number, numberOfAdvertisements: number) {
        this.numberOfAdvertisements = numberOfAdvertisements;
        this.numberOfCompanies = numberOfCompanies;
        this.numberOfPersons = numberOfPersons;
    }
}
