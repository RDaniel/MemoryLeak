import { User } from './user.model';
import { Companyreview } from './companyreview.model';
import { Jobtype } from './jobtype.model';

export class Company extends User {
    public companyName: string;
    public description: string;
    public logoName: string;
    public companyReviews: Companyreview[];
    public jobType: Jobtype;

    constructor (id: number, type: string, name: string, password: string,
        eMail: string, oib: string, address: string, phoneNum: string, regDate: string, userStatus: string,
        role: string[], compName: string, desc: string, logoName: string,
        compRev: Companyreview[], jobType: Jobtype) {
            super(id, type, name, password, eMail, oib, address, phoneNum, regDate, userStatus, role);
            this.companyName = compName;
            this.description = desc;
            this.logoName = logoName;
            this.companyReviews = compRev;
            this.jobType = jobType;
        }
}
