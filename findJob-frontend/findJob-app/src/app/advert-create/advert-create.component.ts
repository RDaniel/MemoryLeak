import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Job } from '../models/job.model';
import { AdvertisementService } from '../services/advertisement.service';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Router } from '@angular/router/';
import { JobtypeService } from '../services/jobtype.service';
import { Jobtype } from '../models/jobtype.model';

@Component({
  selector: 'app-advert-create',
  templateUrl: './advert-create.component.html',
  styleUrls: ['./advert-create.component.css']
})
export class AdvertCreateComponent implements OnInit {
  job: Job;
  user: User;
  adType: string;
  jobTypes: Jobtype[];

  constructor(private adService: AdvertisementService, private userService: UserService,
    private router: Router, private jobTypeService: JobtypeService) { }

  ngOnInit() {
    this.jobTypeService.getAllJobtypes().subscribe(
      (data: any[]) => {
      this.jobTypes = data;
      }
    );
    this.userService.getCurrentUser().subscribe(
      (user: User) => {
        this.user = user;
        if (user.type === 'person') {
          this.adType = 'TRAŽIM';
        } else {
          this.adType = 'NUDIM';
        }
      }
    );
  }

  onSubmit(form: NgForm) {
    console.log('New advertisement has been submitted');
    if (form.value.imagePath === '') { form.value.imagePath = null; }
    if (form.value.expDate === '') { form.value.expDate = null; }
    this.job = new Job(null, form.value.adName, form.value.description, form.value.category, this.adType, form.value.expDate,
      form.value.imagePath, this.user);
    this.adService.createJob(this.job).subscribe(
      (data: any) => {
        this.router.navigate(['/poslovi']);
      },
      err => console.log(err)
    );
  }

}
