package hr.fer.opp.project.findjob.service.email;

import freemarker.template.Configuration;
import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.model.user.UserBan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Configuration freemarkerConfig;

    @Override
    public void sendEmail(List<String> toList, String subject, String text) throws MessagingException {
        MimeMessage mailMessage = mailSender.createMimeMessage();
        mailMessage.setSubject(subject);
        mailMessage.setContent(text, "text/html; charset=UTF-8");
        mailMessage.setSentDate(new Date());

        mailMessage.setRecipients(Message.RecipientType.TO, toList.toString()
                .replace("[", "").replace("]", "").trim());

        mailSender.send(mailMessage);
    }

    @Override
    public void sendUserNotification(User user, List<String> recipients, String subject, String templateName) {
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("user", user);

        createTextAndSendEmail(templateName, modelMap, recipients, subject);
    }

    @Override
    public void sendUserBanNotification(UserBan userBan, List<String> recipients, String subject, String templateName) {
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("user", userBan.getUser());
        modelMap.put("date", userBan.getExpirationDate());
        modelMap.put("reason", userBan.getBanReason());

        createTextAndSendEmail(templateName, modelMap, recipients, subject);

    }

    private void createTextAndSendEmail(String templateName, Map<String, Object> modelMap, List<String> recipients, String subject) {
        try {
            String text = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfig.getTemplate(templateName), modelMap);
            sendEmail(recipients, subject, text);
        } catch (Exception ignored) {
        }
    }
}