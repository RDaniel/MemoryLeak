package hr.fer.opp.project.findjob.service.user;

import hr.fer.opp.project.findjob.dao.user.UserBanRepository;
import hr.fer.opp.project.findjob.dao.user.UserRepository;
import hr.fer.opp.project.findjob.model.user.Role;
import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.model.user.UserBan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserBanRepository userBanRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByName(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        if (user.isDeleted()) {
            throw new BadCredentialsException("Your account is permanently removed from the application.");
        }

        UserBan userBan = userBanRepository.findByUser_IdAndExpirationDateIsAfter(user.getId(), Date.valueOf(LocalDate.now()));
        if (userBan != null) {
            throw new BadCredentialsException("Your account is banned. Reason : " + userBan.getBanReason() + " Ban expiration date : " + userBan.getExpirationDate());
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.toString()));
        }

        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), grantedAuthorities);
    }
}