package hr.fer.opp.project.findjob.model.user.person;

import hr.fer.opp.project.findjob.model.registry.JobType;
import hr.fer.opp.project.findjob.model.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;
import java.sql.Date;

@Entity
public class Person extends User {

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column
    private Date dateOfBirth;

    @Column
    @Size(min = 10)
    private String CV;

    @OneToOne
    @JoinColumn
    private JobType areaOfInterest;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCV() {
        return CV;
    }

    public void setCV(String CV) {
        this.CV = CV;
    }

    public JobType getAreaOfInterest() {
        return areaOfInterest;
    }

    public void setAreasOfInterest(JobType areaOfInterest) {
        this.areaOfInterest = areaOfInterest;
    }
}