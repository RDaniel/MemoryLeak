package hr.fer.opp.project.findjob.model.statistic;

public class PageStatistic {

    private int numberOfPersons;

    private int numberOfCompanies;

    private int numberOfAdvertisements;

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public int getNumberOfCompanies() {
        return numberOfCompanies;
    }

    public void setNumberOfCompanies(int numberOfCompanies) {
        this.numberOfCompanies = numberOfCompanies;
    }

    public int getNumberOfAdvertisements() {
        return numberOfAdvertisements;
    }

    public void setNumberOfAdvertisements(int numberOfAdvertisements) {
        this.numberOfAdvertisements = numberOfAdvertisements;
    }
}