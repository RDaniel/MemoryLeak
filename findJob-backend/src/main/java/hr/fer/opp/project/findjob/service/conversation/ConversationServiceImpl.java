package hr.fer.opp.project.findjob.service.conversation;

import hr.fer.opp.project.findjob.dao.conversation.ConversationRepository;
import hr.fer.opp.project.findjob.dao.conversation.MessageRepository;
import hr.fer.opp.project.findjob.dao.user.UserRepository;
import hr.fer.opp.project.findjob.model.conversation.Conversation;
import hr.fer.opp.project.findjob.model.conversation.Message;
import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Transactional
@Service
public class ConversationServiceImpl implements ConversationService {

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Override
    public Message saveMessage(Message message) {
        message.setSender(userService.getCurrentUser());
        message.setSendDate(Date.valueOf(LocalDate.now()));

        return messageRepository.save(message);
    }

    @Override
    public Conversation getConversation(Long id) {
        return conversationRepository.findOne(id);
    }

    @Override
    public Conversation saveConversation(Conversation conversation) {
        return conversationRepository.save(conversation);
    }

    @Override
    public List<Conversation> getAdvertisementConversations(Long advertisementId) {
        return conversationRepository.findByAdvertisementId(advertisementId);
    }

    @Override
    public Conversation getCurrentUserAdvertisementConversation(Long advertisementId) {
        User currentUser = userService.getCurrentUser();
        return conversationRepository.findByAdvertisementIdAndParticipantsContaining(advertisementId, currentUser);
    }

    @Override
    public void deleteConversation(Long conversationId) {
        conversationRepository.delete(conversationId);
    }

    @Override
    public List<Message> getConversationMessages(Long conversationId) {
        return messageRepository.findByConversationId(conversationId);
    }

    @Override
    public List<Conversation> getUserConversations(Long userId) {
        return conversationRepository.findByParticipantsContaining(userRepository.findOne(userId));
    }
}