package hr.fer.opp.project.findjob.dao.conversation;

import hr.fer.opp.project.findjob.model.conversation.Conversation;
import hr.fer.opp.project.findjob.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Long> {

    List<Conversation> findByParticipantsContaining(User participant);

    List<Conversation> findByAdvertisementId(Long advertisementId);

    Conversation findByAdvertisementIdAndParticipantsContaining(Long advertisementId, User participant);
}