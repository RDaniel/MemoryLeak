package hr.fer.opp.project.findjob.service.user.company;


import hr.fer.opp.project.findjob.model.user.company.Company;
import hr.fer.opp.project.findjob.model.user.company.review.CompanyReview;
import hr.fer.opp.project.findjob.model.user.company.review.CompanyReviewDto;
import hr.fer.opp.project.findjob.web.command.CompanySearchCommand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CompanyService {

    List<CompanyReview> getUserCompanyReviews(Long companyId);

    CompanyReview saveReview(CompanyReviewDto companyReview);

    int getNumberOfCompanies();

    void deleteReview(Long id);

    Page<Company> searchCompanies(CompanySearchCommand companySearchCommand);

    Page<CompanyReview> getCompanyReviews(Pageable pageable);
}