package hr.fer.opp.project.findjob.service.email;

import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.model.user.UserBan;

import javax.mail.MessagingException;
import java.util.List;

public interface EmailService {
    void sendEmail(List<String> toList, String subject, String text) throws MessagingException;

    void sendUserNotification(User user, List<String> recipients, String subject, String templateName);

    void sendUserBanNotification(UserBan userBan, List<String> recipients, String subject, String userBanTemplate);
}
