package hr.fer.opp.project.findjob.service.statistic;

import hr.fer.opp.project.findjob.model.statistic.PageStatistic;

public interface StatisticService {

    PageStatistic getPageStatistic();

}
