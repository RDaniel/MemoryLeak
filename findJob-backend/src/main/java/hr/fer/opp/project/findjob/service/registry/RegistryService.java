package hr.fer.opp.project.findjob.service.registry;

import hr.fer.opp.project.findjob.model.advertisement.AdvertisementType;
import hr.fer.opp.project.findjob.model.registry.JobType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RegistryService {

    List<JobType> getAllJobTypes();

    JobType saveJobType(JobType jobType);

    void deleteJobType(String jobType);

    List<AdvertisementType> getAllAdTypes();
}
