package hr.fer.opp.project.findjob.web.controller.user;

import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.model.user.UserBanDto;
import hr.fer.opp.project.findjob.service.user.UserService;
import hr.fer.opp.project.findjob.web.command.UserSearchCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user/")
public class UserController {

    private final UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("search")
    public Page<User> search(UserSearchCommand searchCommand) {
        return service.searchUsers(searchCommand);
    }

    @GetMapping("get")
    public List<User> searchByUsername(@RequestParam String keyWord) {
        return service.searchByUsernameAndEmail(keyWord);
    }

    @GetMapping("all")
    public List<User> getAll() {
        return service.getAllUsers();
    }

    @GetMapping("current")
    public User getCurrentUser() {
        return service.getCurrentUser();
    }

    @PutMapping("admin/{id}")
    public void addAdminRole(@PathVariable("id") Long id) {
        service.addAdminRole(id);
    }

    @GetMapping("{id}")
    public User get(@PathVariable("id") Long id) {
        return service.getByID(id);
    }

    @GetMapping("name")
    public User getByUsername(@RequestParam("username") String username) {
        return service.getByUsername(username);
    }

    @PostMapping("new")
    public User addNew(@RequestBody User user) {
        return service.save(user);
    }

    @PostMapping("ban")
    public User banUser(@RequestBody UserBanDto userBanDto) {
        return service.banUser(userBanDto);
    }

    @DeleteMapping("ban/{id}")
    public void deleteBan(@PathVariable("id") Long userId) {
        service.deleteUserBan(userId);
    }

    @PutMapping("update")
    public User update(@RequestBody User user) {
        return service.update(user);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Long id) {
        service.delete(id);
    }
}