package hr.fer.opp.project.findjob.model.user.company.review;

import com.fasterxml.jackson.annotation.JsonBackReference;
import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.model.user.company.Company;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class CompanyReview {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private Company company;

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private User author;

    @Column(nullable = false)
    private int grade;

    @Column(nullable = false)
    @Size(min = 3)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}