package hr.fer.opp.project.findjob.dao.registry;

import hr.fer.opp.project.findjob.model.registry.JobType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobTypeRepository extends JpaRepository<JobType, Long> {

    void deleteByJobTypeEquals(String jobType);

    List<JobType> findByDeletedFalse();

    JobType findByJobTypeEquals(String jobType);
}
