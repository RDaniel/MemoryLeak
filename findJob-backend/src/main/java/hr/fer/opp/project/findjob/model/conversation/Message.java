package hr.fer.opp.project.findjob.model.conversation;

import com.fasterxml.jackson.annotation.JsonBackReference;
import hr.fer.opp.project.findjob.model.user.User;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(nullable = false)
    @ManyToOne
    private User sender;

    @JoinColumn(nullable = false)
    @ManyToOne
    private User recipient;

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private Date sendDate;

    @JoinColumn(nullable = false)
    @ManyToOne
    @JsonBackReference
    private Conversation conversation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }
}