package hr.fer.opp.project.findjob.service.user;

import hr.fer.opp.project.findjob.dao.user.UserBanRepository;
import hr.fer.opp.project.findjob.dao.user.UserRepository;
import hr.fer.opp.project.findjob.model.user.*;
import hr.fer.opp.project.findjob.model.user.person.Person;
import hr.fer.opp.project.findjob.service.email.EmailService;
import hr.fer.opp.project.findjob.web.command.UserSearchCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final String WELCOME_MAIL_TEMPLATE = "welcome-mail.ftl";

    private static final String USER_BAN_TEMPLATE = "user-ban.ftl";

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserBanRepository userBanRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Environment environment;

    @Override
    public User save(User user) {
        if (user.getId() != null && userRepository.findOne(user.getId()) != null) {
            return update(user);
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRegistrationDate(Date.valueOf(LocalDate.now()));
        user.setUserStatus(UserStatus.AKTIVAN);
        addUserRoles(user);
        sendWelcomeMail(user);

        return userRepository.save(user);
    }

    @Override
    public void deleteUserBan(Long userId) {
        User user = userRepository.findOne(userId);
        user.setUserStatus(UserStatus.AKTIVAN);

        userRepository.save(user);
        userBanRepository.deleteByUserId(userId);
    }

    @Override
    public void delete(Long id) {
        User user = userRepository.findOne(id);
        user.setDeleted(true);

        userRepository.save(user);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByName(username);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findByDeletedFalse();
    }

    @Override
    public List<User> searchByUsernameAndEmail(String keyWord) {
        return userRepository.findByNameAndEmail(keyWord.toLowerCase());
    }

    @Override
    public Page<User> searchUsers(UserSearchCommand searchCommand) {
        Pageable pageable = new PageRequest(searchCommand.getPageNumber(), searchCommand.getPageSize());
        String keyWord = searchCommand.getKeyWord() != null ? searchCommand.getKeyWord() : "";

        return userRepository.findByNameAndEmail(keyWord.toLowerCase(), pageable);
    }

    @Override
    public User getByID(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User update(User user) {
        user.setPassword(userRepository.findOne(user.getId()).getPassword());
        return userRepository.save(user);
    }

    @Override
    public User getCurrentUser() {
        return getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @Override
    public void addAdminRole(Long id) {
        User user = getByID(id);
        user.getRoles().add(Role.ADMIN);
        update(user);
    }

    @Override
    public User banUser(UserBanDto userBanDto) {
        UserBan userBan = createUserBan(userBanDto);
        sendUserBannedMail(userBan);
        User user = userBan.getUser();
        user.setUserStatus(UserStatus.SUSPENDIRAN);

        userBanRepository.save(userBan);
        return userRepository.save(user);
    }

    @Override
    public int getNumberOfUsers() {
        return userRepository.findByDeletedFalse().size();
    }

    @Scheduled(cron = "0 0 0 1/1 * *")
    public void findExpiredUserBans() {
        List<UserBan> userBans = userBanRepository.findByExpirationDateEquals(Date.valueOf(LocalDate.now()));
        userBans.forEach(userBan -> {
            userBan.getUser().setUserStatus(UserStatus.AKTIVAN);
        });
    }

    private void addUserRoles(User user) {
        Set<Role> roles = new HashSet<>();
        roles.add(user instanceof Person ? Role.PERSON : Role.COMPANY);
        user.setRoles(roles);
    }

    private UserBan createUserBan(UserBanDto userBanDto) {
        UserBan userBan = new UserBan();
        userBan.setUser(userRepository.findOne(userBanDto.getUser()));
        userBan.setExpirationDate(userBanDto.getExpirationDate());
        userBan.setBanReason(userBanDto.getBanReason());

        return userBan;
    }

    private void sendUserBannedMail(UserBan userBan) {
        List<String> recipients = new LinkedList<>();
        recipients.add(userBan.getUser().geteMail());
        String subject = environment.getProperty("email.subject.userBan");

        emailService.sendUserBanNotification(userBan, recipients, subject, USER_BAN_TEMPLATE);
    }

    private void sendWelcomeMail(User user) {
        List<String> recipients = new LinkedList<>();
        recipients.add(user.geteMail());
        String subject = environment.getProperty("email.subject.welcomeMail");

        emailService.sendUserNotification(user, recipients, subject, WELCOME_MAIL_TEMPLATE);
    }
}