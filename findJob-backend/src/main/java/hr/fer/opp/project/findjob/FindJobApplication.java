package hr.fer.opp.project.findjob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FindJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(FindJobApplication.class, args);
    }

}
