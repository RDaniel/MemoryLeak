package hr.fer.opp.project.findjob.web.command;

public class CompanySearchCommand extends AdvertisementSearchCommand {

    private double minGrade;

    public double getMinGrade() {
        return minGrade;
    }

    public void setMinGrade(double minGrade) {
        this.minGrade = minGrade;
    }
}