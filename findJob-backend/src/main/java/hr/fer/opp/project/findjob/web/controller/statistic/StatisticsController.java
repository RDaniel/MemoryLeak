package hr.fer.opp.project.findjob.web.controller.statistic;

import hr.fer.opp.project.findjob.model.statistic.PageStatistic;
import hr.fer.opp.project.findjob.service.statistic.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/statistic/")
public class StatisticsController {

    @Autowired
    private StatisticService statisticService;

    @GetMapping("page")
    public PageStatistic getPageStatistic() {
        return statisticService.getPageStatistic();
    }
}