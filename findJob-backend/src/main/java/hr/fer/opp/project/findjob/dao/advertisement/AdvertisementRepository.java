package hr.fer.opp.project.findjob.dao.advertisement;

import hr.fer.opp.project.findjob.model.advertisement.Advertisement;
import hr.fer.opp.project.findjob.model.advertisement.AdvertisementType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Long> {

    List<Advertisement> findByOwnerIdAndDeletedFalse(Long id);

    List<Advertisement> findByDeletedFalse();

    @Query("SELECT ad FROM Advertisement ad JOIN ad.owner ow WHERE " +
            "(LOWER(ad.name) LIKE CONCAT('%',:keyWord, '%') OR " +
            "LOWER(ow.companyName) LIKE CONCAT('%',:keyWord, '%') OR " +
            "LOWER(ow.address) LIKE CONCAT('%',:keyWord, '%'))" +
            "AND (ad.jobType.id = :category OR :category IS NULL)" +
            "AND (ad.type = :adType OR :adType IS NULL)" +
            "AND ad.deleted = false ")
    Page<Advertisement> findByKeyWordAndCategory(@Param("keyWord") String keyWord,
                                                 @Param("category") Long category,
                                                 @Param("adType") AdvertisementType adType,
                                                 Pageable pageable);

}