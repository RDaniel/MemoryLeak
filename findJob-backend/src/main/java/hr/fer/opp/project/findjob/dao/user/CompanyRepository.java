package hr.fer.opp.project.findjob.dao.user;

import hr.fer.opp.project.findjob.model.user.company.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query("SELECT co FROM Company co WHERE " +
            "(LOWER(co.companyName) LIKE CONCAT('%',:keyWord, '%') OR " +
            "LOWER(co.address) LIKE CONCAT('%',:keyWord, '%')) " +
            "AND (co.jobType.id = :category OR :category IS NULL)" +
            "AND (co.averageGrade >= :minGrade)" +
            "AND co.deleted = FALSE")
    Page<Company> findByKeyWordCategoryAndGrade(@Param("keyWord") String keyWord, @Param("category") Long category, @Param("minGrade") Double minGrade, Pageable pageable);

    List<Company> findAllByDeletedFalse();
}