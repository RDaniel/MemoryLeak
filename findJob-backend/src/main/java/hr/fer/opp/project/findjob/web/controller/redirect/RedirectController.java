package hr.fer.opp.project.findjob.web.controller.redirect;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectController implements ErrorController {

    @RequestMapping({"/error"})
    public String index() {
        return "forward:/index.html";
    }

    @Override
    public String getErrorPath() {
        return "index.html";
    }
}