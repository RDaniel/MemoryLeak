package hr.fer.opp.project.findjob.service.statistic;

import hr.fer.opp.project.findjob.model.statistic.PageStatistic;
import hr.fer.opp.project.findjob.service.advertisement.AdvertisementService;
import hr.fer.opp.project.findjob.service.user.UserService;
import hr.fer.opp.project.findjob.service.user.company.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AdvertisementService advertisementService;

    @Override
    public PageStatistic getPageStatistic() {
        int numberOfCompanies = companyService.getNumberOfCompanies();
        int numberOfUsers = userService.getNumberOfUsers();

        PageStatistic pageStatistic = new PageStatistic();
        pageStatistic.setNumberOfCompanies(numberOfCompanies);
        pageStatistic.setNumberOfPersons(numberOfUsers - numberOfCompanies);
        pageStatistic.setNumberOfAdvertisements(advertisementService.getNumberOfAdvertisements());

        return pageStatistic;
    }
}
