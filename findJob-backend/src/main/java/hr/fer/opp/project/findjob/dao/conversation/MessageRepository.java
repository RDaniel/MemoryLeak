package hr.fer.opp.project.findjob.dao.conversation;

import hr.fer.opp.project.findjob.model.conversation.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findByConversationId(Long conversationId);

}