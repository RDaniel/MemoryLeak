package hr.fer.opp.project.findjob.service.conversation;

import hr.fer.opp.project.findjob.model.conversation.Conversation;
import hr.fer.opp.project.findjob.model.conversation.Message;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ConversationService {

    Message saveMessage(Message message);

    Conversation getCurrentUserAdvertisementConversation(Long advertisementId);

    void deleteConversation(Long conversationId);

    List<Message> getConversationMessages(Long conversationId);

    List<Conversation> getUserConversations(Long userId);

    Conversation saveConversation(Conversation conversation);

    List<Conversation> getAdvertisementConversations(Long advertisementId);

    Conversation getConversation(Long id);
}