package hr.fer.opp.project.findjob.web.command;

import hr.fer.opp.project.findjob.model.advertisement.AdvertisementType;

public class AdvertisementSearchCommand {

    private int pageNumber;

    private int pageSize;

    private String keyWord;

    private Long categoryId;

    private AdvertisementType adType;

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Long getCategory() {
        return categoryId;
    }

    public void setCategory(Long categoryId) {
        this.categoryId = categoryId;
    }

    public AdvertisementType getAdType() {
        return adType;
    }

    public void setAdType(AdvertisementType adType) {
        this.adType = adType;
    }
}