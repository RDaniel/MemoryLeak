package hr.fer.opp.project.findjob.dao.user;

import hr.fer.opp.project.findjob.model.user.UserBan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface UserBanRepository extends JpaRepository<UserBan, Long> {

    UserBan findByUser_IdAndExpirationDateIsAfter(Long id, Date date);

    List<UserBan> findByExpirationDateEquals(Date date);

    void deleteByUserId(Long userId);

}