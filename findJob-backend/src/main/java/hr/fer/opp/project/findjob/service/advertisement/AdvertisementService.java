package hr.fer.opp.project.findjob.service.advertisement;

import hr.fer.opp.project.findjob.model.advertisement.Advertisement;
import hr.fer.opp.project.findjob.web.command.AdvertisementSearchCommand;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdvertisementService {

    Advertisement getAdvertisementById(Long id);

    List<Advertisement> getAllAdvertisements();

    List<Advertisement> getUserAdvertisements(Long id);

    Advertisement addNewAdvertisement(Advertisement advertisement);

    void deleteAdvertisement(Long id);

    Page<Advertisement> getAdvertisements(AdvertisementSearchCommand searchCommand);

    int getNumberOfAdvertisements();

    @Scheduled(cron = "0 0/1 * 1/1 * *")
    void findExpiredAdvertisements();
}
