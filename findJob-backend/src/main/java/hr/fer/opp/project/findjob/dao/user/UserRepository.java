package hr.fer.opp.project.findjob.dao.user;

import hr.fer.opp.project.findjob.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    String QUERY = "SELECT us FROM User us WHERE deleted = FALSE " +
            "AND (LOWER(us.name) LIKE CONCAT('%',:keyword, '%') " +
            "OR LOWER(us.eMail) LIKE CONCAT('%',:keyword,'%'))";

    List<User> findByDeletedFalse();

    User findByName(String name);

    @Query(QUERY)
    List<User> findByNameAndEmail(@Param("keyword") String key);

    @Query(QUERY)
    Page<User> findByNameAndEmail(@Param("keyword") String key, Pageable pageable);

}