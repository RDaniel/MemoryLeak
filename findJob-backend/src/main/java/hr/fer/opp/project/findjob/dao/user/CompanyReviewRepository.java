package hr.fer.opp.project.findjob.dao.user;

import hr.fer.opp.project.findjob.model.user.company.review.CompanyReview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyReviewRepository extends JpaRepository<CompanyReview, Long> {

    List<CompanyReview> findByCompanyId(Long companyID);

}