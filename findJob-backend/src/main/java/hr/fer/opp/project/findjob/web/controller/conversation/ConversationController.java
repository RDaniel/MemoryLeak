package hr.fer.opp.project.findjob.web.controller.conversation;

import hr.fer.opp.project.findjob.model.conversation.Conversation;
import hr.fer.opp.project.findjob.model.conversation.Message;
import hr.fer.opp.project.findjob.service.conversation.ConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/conversation/")
public class ConversationController {

    @Autowired
    private ConversationService conversationService;

    @GetMapping("{id}")
    public Conversation getConversation(@PathVariable("id") Long id) {
        return conversationService.getConversation(id);
    }

    @GetMapping("all")
    public List<Conversation> getUserConversations(@RequestParam("id") Long userId) {
        return conversationService.getUserConversations(userId);
    }

    @GetMapping("ad/{id}")
    public List<Conversation> getAdvertisementConversations(@RequestParam("id") Long advertisementId) {
        return conversationService.getAdvertisementConversations(advertisementId);
    }

    @GetMapping("message/{id}")
    public List<Message> getConversationMessages(@PathVariable("id") Long conversationId) {
        return conversationService.getConversationMessages(conversationId);
    }

    @GetMapping("ad/current/{id}")
    public Conversation getCurrentUserAdvertisementConversation(@PathVariable("id") Long advertisementId) {
        return conversationService.getCurrentUserAdvertisementConversation(advertisementId);
    }

    @PostMapping("new")
    public Conversation addConversation(@RequestBody Conversation conversation) {
        return conversationService.saveConversation(conversation);
    }

    @PostMapping("message/new")
    public Message addMessage(@RequestBody Message message) {
        return conversationService.saveMessage(message);
    }

    @DeleteMapping("{id}")
    public void deleteConversation(@PathVariable("id") Long conversationId) {
        conversationService.deleteConversation(conversationId);
    }
}