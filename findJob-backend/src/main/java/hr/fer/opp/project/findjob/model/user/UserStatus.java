package hr.fer.opp.project.findjob.model.user;

public enum UserStatus {
    AKTIVAN, SUSPENDIRAN
}