package hr.fer.opp.project.findjob.web.controller.advertisement;

import hr.fer.opp.project.findjob.model.advertisement.Advertisement;
import hr.fer.opp.project.findjob.service.advertisement.AdvertisementService;
import hr.fer.opp.project.findjob.web.command.AdvertisementSearchCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/advertisement/")
public class AdvertisementController {

    @Autowired
    private AdvertisementService advertisementService;

    @GetMapping("all")
    public Page<Advertisement> getAllAdvertisements(AdvertisementSearchCommand searchCommand) {
        return advertisementService.getAdvertisements(searchCommand);
    }

    @GetMapping("{id}")
    public Advertisement get(@PathVariable("id") Long id) {
        return advertisementService.getAdvertisementById(id);
    }

    @GetMapping("owner")
    public List<Advertisement> getUserAdvertisements(@RequestParam("id") Long id) {
        return advertisementService.getUserAdvertisements(id);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Long id) {
        advertisementService.deleteAdvertisement(id);
    }

    @PostMapping("new")
    public void addNewAdvertisement(@RequestBody Advertisement advertisement) {
        advertisementService.addNewAdvertisement(advertisement);
    }
}