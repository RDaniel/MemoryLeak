package hr.fer.opp.project.findjob.web.controller.user;

import hr.fer.opp.project.findjob.model.user.company.Company;
import hr.fer.opp.project.findjob.model.user.company.review.CompanyReview;
import hr.fer.opp.project.findjob.model.user.company.review.CompanyReviewDto;
import hr.fer.opp.project.findjob.service.user.company.CompanyService;
import hr.fer.opp.project.findjob.web.command.CompanySearchCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user/company/")
public class CompanyController {

    private CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("all")
    public Page<Company> getCompanies(CompanySearchCommand companySearchCommand) {
        return companyService.searchCompanies(companySearchCommand);
    }

    @GetMapping("review/all")
    public Page<CompanyReview> getCompanyReviews(Pageable pageable) {
        return companyService.getCompanyReviews(pageable);
    }


    @GetMapping("review/{id}")
    public List<CompanyReview> getCompanyReviews(@PathVariable("id") Long id) {
        return companyService.getUserCompanyReviews(id);
    }

    @PostMapping("review/new")
    public CompanyReview addCompanyReview(@RequestBody CompanyReviewDto companyReview) {
        return companyService.saveReview(companyReview);
    }

    @DeleteMapping("review/{id}")
    public void deleteCompanyReview(@PathVariable("id") Long id) {
        companyService.deleteReview(id);
    }
}