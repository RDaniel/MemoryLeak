package hr.fer.opp.project.findjob.service.user.company;

import hr.fer.opp.project.findjob.dao.user.CompanyRepository;
import hr.fer.opp.project.findjob.dao.user.CompanyReviewRepository;
import hr.fer.opp.project.findjob.dao.user.UserRepository;
import hr.fer.opp.project.findjob.model.user.company.Company;
import hr.fer.opp.project.findjob.model.user.company.review.CompanyReview;
import hr.fer.opp.project.findjob.model.user.company.review.CompanyReviewDto;
import hr.fer.opp.project.findjob.service.email.EmailService;
import hr.fer.opp.project.findjob.web.command.CompanySearchCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    private static final String REVIEW_ADDED_TEMPLATE = "company-review-added.ftl";

    private final CompanyReviewRepository companyReviewRepository;

    private final UserRepository userRepository;

    private final Environment environment;

    private CompanyRepository companyRepository;

    private EmailService emailService;

    @Autowired
    public CompanyServiceImpl(CompanyReviewRepository companyReviewRepository, UserRepository userRepository, CompanyRepository companyRepository, EmailService emailService, Environment environment) {
        this.companyReviewRepository = companyReviewRepository;
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.emailService = emailService;
        this.environment = environment;
    }

    @Override
    public Page<CompanyReview> getCompanyReviews(Pageable pageable) {
        return companyReviewRepository.findAll(pageable);
    }

    public List<CompanyReview> getUserCompanyReviews(Long companyId) {
        return companyReviewRepository.findByCompanyId(companyId);
    }

    @Override
    public CompanyReview saveReview(CompanyReviewDto companyReviewDto) {
        CompanyReview companyReview = new CompanyReview();
        Company company = companyRepository.findOne(companyReviewDto.getCompany());
        createCompanyReview(companyReviewDto, companyReview, company);

        company.setAverageGrade(calculateCompanyAverageGrade(company, companyReview));
        userRepository.save(company);
        sendReviewAddedNotification(company);

        return companyReviewRepository.save(companyReview);
    }

    @Override
    public Page<Company> searchCompanies(CompanySearchCommand searchCommand) {
        Pageable pageable = new PageRequest(searchCommand.getPageNumber(), searchCommand.getPageSize());
        String keyWord = searchCommand.getKeyWord() != null ? searchCommand.getKeyWord().toLowerCase() : "";
        return companyRepository.findByKeyWordCategoryAndGrade(keyWord, searchCommand.getCategory(), searchCommand.getMinGrade(), pageable);
    }

    @Override
    public int getNumberOfCompanies() {
        return companyRepository.findAllByDeletedFalse().size();
    }

    @Override
    public void deleteReview(Long id) {
        companyReviewRepository.delete(id);
    }

    private void createCompanyReview(CompanyReviewDto companyReviewDto, CompanyReview companyReview, Company company) {
        companyReview.setDescription(companyReviewDto.getDescription());
        companyReview.setGrade(companyReviewDto.getGrade());
        companyReview.setCompany(company);
        companyReview.setAuthor(userRepository.findOne(companyReviewDto.getAuthor()));
    }

    private void sendReviewAddedNotification(Company company) {
        List<String> recipients = new ArrayList<>();
        recipients.add(company.geteMail());
        String subject = environment.getProperty("email.subject.companyReview");

        emailService.sendUserNotification(company, recipients, subject, REVIEW_ADDED_TEMPLATE);
    }

    private double calculateCompanyAverageGrade(Company company, CompanyReview newReview) {
        List<CompanyReview> companyReviews = company.getCompanyReviews();
        if (companyReviews == null) {
            companyReviews = Collections.emptyList();
        }
        companyReviews.add(newReview);

        OptionalDouble average = companyReviews.stream().mapToDouble(CompanyReview::getGrade).average();
        return average.isPresent() ? average.getAsDouble() : 0;
    }
}