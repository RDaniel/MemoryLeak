package hr.fer.opp.project.findjob.model.user;

public enum Role {
    PERSON, COMPANY, ADMIN
}