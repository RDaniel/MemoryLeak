package hr.fer.opp.project.findjob.service.user;

import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.model.user.UserBanDto;
import hr.fer.opp.project.findjob.web.command.UserSearchCommand;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    void addAdminRole(Long id);

    void deleteUserBan(Long userBanId);

    void delete(Long id);

    User getByUsername(String username);

    Page<User> searchUsers(UserSearchCommand searchCommand);

    User getByID(Long id);

    User save(User user);

    User update(User user);

    User banUser(UserBanDto userBan);

    User getCurrentUser();

    int getNumberOfUsers();

    List<User> getAllUsers();

    List<User> searchByUsernameAndEmail(String username);
}