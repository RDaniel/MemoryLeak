package hr.fer.opp.project.findjob.service.registry;

import hr.fer.opp.project.findjob.dao.registry.JobTypeRepository;
import hr.fer.opp.project.findjob.model.advertisement.AdvertisementType;
import hr.fer.opp.project.findjob.model.registry.JobType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RegistryServiceImpl implements RegistryService {

    @Autowired
    private JobTypeRepository jobTypeRepository;

    @Override
    public List<JobType> getAllJobTypes() {
        return jobTypeRepository.findByDeletedFalse();
    }

    @Override
    public JobType saveJobType(JobType jobType) {
        return jobTypeRepository.save(jobType);
    }

    @Override
    public void deleteJobType(String jobTypeName) {
        JobType jobType = jobTypeRepository.findByJobTypeEquals(jobTypeName);
        jobType.setDeleted(true);
        jobTypeRepository.save(jobType);
    }

    @Override
    public List<AdvertisementType> getAllAdTypes() {
        return Arrays.stream(AdvertisementType.values()).collect(Collectors.toList());
    }
}