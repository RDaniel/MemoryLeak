package hr.fer.opp.project.findjob.model.advertisement;

public enum AdvertisementType {
    TRAŽIM, NUDIM
}
