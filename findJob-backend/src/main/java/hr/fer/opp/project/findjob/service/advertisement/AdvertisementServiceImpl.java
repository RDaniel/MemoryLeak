package hr.fer.opp.project.findjob.service.advertisement;

import hr.fer.opp.project.findjob.dao.advertisement.AdvertisementRepository;
import hr.fer.opp.project.findjob.model.advertisement.Advertisement;
import hr.fer.opp.project.findjob.web.command.AdvertisementSearchCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {

    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Override
    public Advertisement getAdvertisementById(Long id) {
        return advertisementRepository.findOne(id);
    }

    @Override
    public List<Advertisement> getAllAdvertisements() {
        return advertisementRepository.findByDeletedFalse();
    }

    @Override
    public List<Advertisement> getUserAdvertisements(Long id) {
        return advertisementRepository.findByOwnerIdAndDeletedFalse(id);
    }

    @Override
    public Advertisement addNewAdvertisement(Advertisement advertisement) {
        advertisement.setCreatedOn(System.currentTimeMillis());
        return advertisementRepository.save(advertisement);
    }

    @Override
    public void deleteAdvertisement(Long id) {
        Advertisement advertisement = getAdvertisementById(id);
        advertisement.setDeleted(true);

        advertisementRepository.save(advertisement);
    }

    @Override
    public Page<Advertisement> getAdvertisements(AdvertisementSearchCommand searchCommand) {
        Pageable pageable = new PageRequest(searchCommand.getPageNumber(), searchCommand.getPageSize(), Sort.Direction.DESC, "createdOn");
        String keyWord = searchCommand.getKeyWord() != null ? searchCommand.getKeyWord().toLowerCase() : "";
        return advertisementRepository.findByKeyWordAndCategory(keyWord, searchCommand.getCategory(), searchCommand.getAdType(), pageable);
    }

    @Override
    public int getNumberOfAdvertisements() {
        return advertisementRepository.findAll().size();
    }

    @Scheduled(cron = "0 0 0 1/1 * *")
    public void findExpiredAdvertisements() {
        List<Advertisement> allAdvertisements = getAllAdvertisements();

        for (Advertisement advertisement : allAdvertisements) {
            if (advertisement.getExpirationDate() == null) {
                continue;
            }

            LocalDate expirationDate = advertisement.getExpirationDate().toLocalDate();
            if (expirationDate.isEqual(LocalDate.now()) || expirationDate.isBefore(LocalDate.now())) {
                advertisement.setDeleted(true);
                advertisementRepository.save(advertisement);
            }
        }
    }
}