package hr.fer.opp.project.findjob.model.user.company;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import hr.fer.opp.project.findjob.model.registry.JobType;
import hr.fer.opp.project.findjob.model.user.User;
import hr.fer.opp.project.findjob.model.user.company.review.CompanyReview;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Company extends User {

    @Column(nullable = false)
    private String companyName;

    @Column(nullable = false)
    @Size(min = 10)
    private String description;

    @Column
    private String logoName;

    @OneToMany(mappedBy = "company", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private List<CompanyReview> companyReviews;

    @OneToOne
    @JoinColumn(nullable = false)
    private JobType jobType;

    @Column
    private Double averageGrade = 0.0;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    public List<CompanyReview> getCompanyReviews() {
        return companyReviews;
    }

    public void setCompanyReviews(List<CompanyReview> companyReviews) {
        this.companyReviews = companyReviews;
    }

    public Double getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(Double averageGrade) {
        this.averageGrade = averageGrade;
    }
}