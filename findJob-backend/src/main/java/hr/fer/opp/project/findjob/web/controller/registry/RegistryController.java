package hr.fer.opp.project.findjob.web.controller.registry;

import hr.fer.opp.project.findjob.model.advertisement.AdvertisementType;
import hr.fer.opp.project.findjob.model.registry.JobType;
import hr.fer.opp.project.findjob.service.registry.RegistryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/registry/")
public class RegistryController {

    @Autowired
    private RegistryService registryService;

    @RequestMapping("jobTypes")
    public List<JobType> getAllJobTypes() {
        return registryService.getAllJobTypes();
    }

    @PostMapping("jobTypes")
    public JobType saveJobType(@RequestBody JobType jobType) {
        return registryService.saveJobType(jobType);
    }

    @DeleteMapping("jobTypes")
    public void deleteJobType(@RequestParam("name") String jobType) {
        registryService.deleteJobType(jobType);
    }

    @RequestMapping("adTypes")
    public List<AdvertisementType> getAllAdTypes() {
        return registryService.getAllAdTypes();
    }
}